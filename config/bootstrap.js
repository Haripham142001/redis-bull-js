const { initEventQueues, initProducers } = require("../services/eventStream");

const bootstrap = async () => {
  await initProducers();
  await initEventQueues();
  console.log("load bootstrap");
};

module.exports = bootstrap;
