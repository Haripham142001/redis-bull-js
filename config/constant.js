exports.MONGODB_URI = "mongodb://localhost:27017/learn-redis";
exports.REDIS_CONFIG = {
  redis: {
    port: 6379,
    host: "127.0.0.1",
  },
};
exports.EVENT = ["emailQueue", "helloWorld"];
