const Queue = require("bull");
const path = require("path");
const { REDIS_CONFIG, EVENT } = require("../config/constant");
const queues = {};

const initProducers = () => {
  EVENT.forEach((evt) => {
    queues[evt] = new Queue(evt, REDIS_CONFIG);
  });
  setTimeout(async () => {
    await initProducers();
  }, 1 * 60 * 1000);
};

const publishEvent = (evtName, evtData) => {
  queues[evtName].add(evtData);
};

const subscribeEvent = (evtName) => {
  queues[evtName].process(
    path.join(__dirname, `./processors/${evtName}Processor.js`)
  );
  queues[evtName].on("completed", (job) => {
    console.log(`Completed job ${evtName}-#${job.id}`);
  });
};

const initEventQueues = () => {
  EVENT.forEach((evt) => {
    subscribeEvent(evt);
  });
  setTimeout(async () => {
    await initEventQueues();
  }, 1 * 60 * 1000);
};

module.exports = {
  publishEvent,
  subscribeEvent,
  initEventQueues,
  initProducers,
};
