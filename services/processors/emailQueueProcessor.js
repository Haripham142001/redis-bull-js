const sendEmailCreationEmail = require("../../mail/sendAccountCreationEmail");

const emailQueueProcessor = async (job, done) => {
  await doSendEmail(job.data);
  done();
};

const doSendEmail = async (evt) => {
  await sendEmailCreationEmail({ email: evt.email, name: evt.name });
};

module.exports = emailQueueProcessor;
