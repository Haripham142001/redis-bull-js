const helloWorldProcessor = async (job, done) => {
  await doHelloWorld(job.data);
  done();
};

const doHelloWorld = async (evt) => {
  console.log("Hello world" + Math.random());
};

module.exports = helloWorldProcessor;
