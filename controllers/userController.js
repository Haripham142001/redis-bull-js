const User = require("../models/User");
const sendEmailCreationEmail = require("../mail/sendAccountCreationEmail");
const eventStream = require("../services/eventStream");

exports.create = async (req, res) => {
  const { name, email } = req.body;

  try {
    const user = await User.create({
      name,
      email,
    });

    await sendEmailCreationEmail({ name, email });

    res.json(user);
  } catch (error) {
    console.log(error);
    res.status(400).json(error);
  }
};

exports.sendEmailToUsers = async (req, res) => {
  try {
    const users = await User.find();

    users.forEach((user) => {
      eventStream.publishEvent("emailQueue", {
        email: user.email,
        name: user.name,
      });
    });
    eventStream.publishEvent("helloWorld", {});
    return res.json({ message: "Pending" });
  } catch (error) {
    console.log(error);
    res.status(400).json(error);
  }
};
